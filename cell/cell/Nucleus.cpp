/*********************************
* Class: MAGSHIMIM C2			 *
* Week:                			 *
* Name:                          *
* Credits:                       *
**********************************/

#include <stdio.h>
#include <iostream>
#include <new>
#include "Nucleus.h"
#include "Protein.h"
#include"AminoAcid.h"
#include <string>
using namespace std;

/*
Function init a gene
input: start end and check if on gene
output: none
*/
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	if (end < start)
	{
		this->_end = this->_start + 1;
		cerr << "end cant be smaller then the start set end to start + +1\n";
	}
	else
	{
		this->_end = end;
	}
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}
/*
Function check if on gene
input: none
output: true/false
*/
bool Gene::is_on_complementary_dna_strand()const
{
	return this->_on_complementary_dna_strand;
}
/*
Function get the start of the gene
input: none
output: start of gene
*/
unsigned int Gene::get_start()const
{
	return this->_start;
}
/*
Function get the end of the gene
input: none
output: end of gene
*/
unsigned int Gene::get_end()const
{
	return this->_end;
}
/*
Function set if on gene
input: the check if on gene
output: none
*/
void Gene::set_on_complementary_dna_strand(const bool on_complementary_dna_strand) 
{
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}
/*
Function set the start of the gene
input: start of gene
output: none 
*/
void Gene::set_start(const unsigned int start)
{
	if (start > this->_end)
	{
		cerr << "\nstart cant be bigger then no modifications were made\n";
	}
	else 
	{

		this->_start = start;
	}
	
}
/*
Function set the end of the gene
input: end of gene
output: none
*/
void Gene::set_end(const unsigned int end) 
{
	if (end < this->_start)
	{
		cerr << "\nend cant be smaller then no modifications were made\n";
	}
	else 
	{
		this->_end = end;
	}
}
/*
Function init Nucleus
input: dna_sequence
output: none
*/
void Nucleus::init(const string dna_sequence)
{
	int i = 0;
	this->_DNA_strand = dna_sequence;
	int size = this->_DNA_strand.size();

	for (i = 0; i < size; i++)
	{
		if (this->_DNA_strand[i] == 'G')
		{
			this->_complementary_DNA_strand += 'C';
		}
		else if (this->_DNA_strand[i] == 'C')
		{
			this->_complementary_DNA_strand += 'G';
		}
		else if (this->_DNA_strand[i] == 'A')
		{
			this->_complementary_DNA_strand += 'T';
		}
		else if (this->_DNA_strand[i] == 'T')
		{
			this->_complementary_DNA_strand += 'A';
		}
		else
		{
			cerr << "DNA contains only A.T.C.G";
			exit(1);
		}
	}

}

/*
Function make RNA_transcript
input: gene
output: the RNA_transcript
*/
string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	int i = 0;
	unsigned int start = gene.get_start();
	unsigned int end = gene.get_end();
	string rna = "";
	if (gene.is_on_complementary_dna_strand())
	{
		for (i = start; i <= end; i++)
		{
			if (this->_complementary_DNA_strand[i] == 'T') 
			{
				rna += 'U';
			}
			else
			{
				rna += this->_complementary_DNA_strand[i];
			}
		}
		
	}
	else
	{
		for (i = start; i <= end; i++)
		{
			if (this->_DNA_strand[i] == 'T')
			{
				rna += 'U';
			}
			else
			{
				rna += this->_DNA_strand[i];
			}
		}

	}
	return rna;

}
/*
Function reverse dna
input: none
output: the reversed dna
*/
string Nucleus::get_reversed_DNA_strand() const
{
	string re = this->_DNA_strand;
	reverse(re.begin(), re.end());
	return re;
}
/*
Function find how many times codon in dna
input: the codon
output: number of times
*/
unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const 
{
	string save = this->_DNA_strand;
	size_t place = 0;
	int count = 0;

	while (save.find(codon) != 4294967295)
	{
		 save[save.find(codon)] = ' ';
		 count++;
	}
	return count;
}