#include <stdio.h>
#include <iostream>
#include <new>
#include "Nucleus.h"
#include "Protein.h"
#include "AminoAcid.h"
#include "Ribosome.h"
#include <string>
using namespace std;

/*
Function create protein 
input:rna
output: the protein
*/
Protein* Ribosome::create_protein(std::string& RNA_transcript) const
{
	Protein* p;
	p = new Protein;
	p->init();
	string three = "";
	AminoAcid save = UNKNOWN;
	while (RNA_transcript.size() >= 3)
	{
		three = RNA_transcript.substr(0, 3);
		RNA_transcript.erase(0, 3);
		
		save = get_amino_acid(three);
		
		if (save == UNKNOWN) 
		{
			p->clear();
			return nullptr;
		}
		else 
		{
			p->add(save);
		}
	}
	return p;

}