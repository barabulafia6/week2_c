#include <stdio.h>
#include <iostream>
#include <new>
#include "Nucleus.h"
#include "Protein.h"
#include "AminoAcid.h"
#include "Ribosome.h"
#include <string>
#include "Mitochondrion.h"
#include "Cell.h"
using namespace std;
/*
Function init cell
input: glocuse gene , dna 
output: none
*/
void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_nucleus.init(dna_sequence);
	this->_glocus_receptor_gene=glucose_receptor_gene;
	this->_mitochondrion.init();

}
/*
Function get atp
input: non
output: true/falses
*/
bool Cell::get_ATP()
{
	string rna = "";
	Protein* p = nullptr;
	rna = this->_nucleus.get_RNA_transcript(this->_glocus_receptor_gene);
	p = this->_ribosome.create_protein(rna);
	if (p == nullptr) 
	{
		cerr << "can't made cell";
		_exit(1);
	}
	this->_mitochondrion.insert_glucose_receptor(*p);
	this->_mitochondrion.set_glucose(50);
	p->clear();
	if (this->_mitochondrion.produceATP())
	{
		this->_atp_units = 100;
		return true;
	}
	return true;

}