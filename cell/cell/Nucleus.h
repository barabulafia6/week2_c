#ifndef NUCLEUS_H
#define NUCLEUS_H

using namespace std;
class Gene
{
public:
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);
	bool is_on_complementary_dna_strand()const;
	unsigned int get_start()const;
	unsigned int get_end()const;
	void set_on_complementary_dna_strand(const bool on_complementary_dna_strand);
	void set_start(const unsigned int start);
	void set_end(const unsigned int end);
private:
	unsigned int _start;
	unsigned int _end;
	bool _on_complementary_dna_strand;
};
class Nucleus
{
public:
	void init(const string dna_sequence);
	string get_RNA_transcript(const Gene& gene) const;
	string get_reversed_DNA_strand() const;
	unsigned int get_num_of_codon_appearances(const std::string& codon) const;
private:
	string _DNA_strand;
	string _complementary_DNA_strand;
};


#endif