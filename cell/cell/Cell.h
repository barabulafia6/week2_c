#ifndef CELL_H
#define CELL_H
#include <stdio.h>
#include <iostream>
#include <new>
#include "Nucleus.h"
#include "Protein.h"
#include "AminoAcid.h"
#include "Ribosome.h"
#include <string>
#include "Mitochondrion.h"

using namespace std;
class Cell
{
public:
	void init(const std::string dna_sequence, const Gene glucose_receptor_gene);
	bool get_ATP();
private:
	Nucleus _nucleus;
	Ribosome _ribosome;
	Mitochondrion _mitochondrion;
	Gene _glocus_receptor_gene;
	unsigned int _atp_units;
};
#endif // !CELL_H
