#include <stdio.h>
#include <iostream>
#include <new>
#include "Nucleus.h"
#include "Protein.h"
#include "AminoAcid.h"
#include "Ribosome.h"
#include <string>
#include "Mitochondrion.h"
using namespace std;
/*
Function init Mitochondrion
input: none
output: none
*/
void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}
/*
Function insert_glucose_receptor
input: protein
output: none
*/
void Mitochondrion::insert_glucose_receptor(const Protein& protein)
{
	AminoAcidNode* a;
	a = new AminoAcidNode;
	
	a = protein.get_first();
	if (a->get_data() == ALANINE) 
	{
		a = a->get_next();
		if (a->get_data() == LEUCINE)
		{
			a = a->get_next();
			if (a->get_data() == GLYCINE)
			{
				a = a->get_next();
				if (a->get_data() == HISTIDINE)
				{
					a = a->get_next();
					if (a->get_data() == LEUCINE)
					{
						a = a->get_next();
						if (a->get_data() == PHENYLALANINE)
						{
							a = a->get_next();
							if (a->get_data() == AMINO_CHAIN_END)
							{
								this->_has_glocuse_receptor = true;
							}

						}
					}
				}
			}
		}
	}

}
/*
Function set glucose
input: glocuse_units
output: none
*/
void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}
/*
Function produce ATP
input: none
output: true/false
*/
bool Mitochondrion::produceATP() const 
{
	if (this->_has_glocuse_receptor && this->_glocuse_level >= 50) 
	{
		return true;
	}
	return false;
}